﻿using Microsoft.EntityFrameworkCore;
using NotesApplictaion.Interfaces;
using NotesPersistence.EntityTypeConfiguration;
using NotestDomain;

namespace NotesPersistence
{
    public class NotesDbContext : DbContext, INotesDbContext
    {
        public DbSet<Note> Notes { get; set; }
        public NotesDbContext(DbContextOptions<NotesDbContext> options) : base(options){ }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new NoteConfiguration());
            base.OnModelCreating(builder);
        }
    }
}
